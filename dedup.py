#!/usr/bin/env python3

import glob
import subprocess


def should_dedup(filename):
    patterns = ["*.deb", "*.dsc", "*.tar.*"]
    for p in patterns:
        if glob.fnmatch.fnmatch(filename, p):
            return True
    return False


dups = subprocess.check_output(
    [
        "fdupes",
        "-r",
        "public/release",
        "public/archive",
        "public/daily",
        "public/snapshot",
    ],
    text=True,
)
source = None
dest = None
for f in dups.split("\n"):
    if f:
        if source:
            dest = f
        else:
            source = f
    else:
        source = None
        dest = None
    if source and dest and should_dedup(source) and should_dedup(dest):
        print(f"{source} -> {dest}")
        subprocess.check_call(["ln", "-sfrT", source, dest])
