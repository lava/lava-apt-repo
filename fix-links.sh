#!/bin/sh

set -eu

cd public

for link in $(find . -type l); do
  linkname=$(basename "$link")
  target=$(readlink -f "$link" || true)
  targetname=$(basename "$target")
  if [ "$linkname" != "$targetname" ]; then
    echo I: incorrect link "$link, fixing"
    newtarget=$(find -type f -name "$linkname" || true)
    if [ -n "$newtarget" ]; then
      ln -sfrT "$newtarget" "$link"
    else
      echo "E: could not find canonical copy of $linkname, deleting"
      rm -f "$link"
    fi
  fi
done
