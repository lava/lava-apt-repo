#!/bin/sh

set -eu

channel=${1:-}
case "${channel}" in
  release|daily)
    : OK
    ;;
  *)
    echo "usage: $0 release|daily"
    exit 1
    ;;
esac

set -x

project_id=50269178
if ! [ -d public ]; then
  # download and uncompress latest artifacts
  if wget -O artifacts.zip https://gitlab.com/api/v4/projects/${project_id}/jobs/artifacts/main/download?job=pages; then
    unzip artifacts.zip
  else
    mkdir public
  fi
  rm -f artifacts.zip
fi

mkdir -p public/release public/archive public/daily public/snapshot
basedir=public/"${channel}"
current_version=$(reprepro -b ${basedir} ls lava | awk '{print($3); exit}' | cut -d + -f 1 || true)
archive=
if [ -n "${current_version}" ]; then
  case "${channel}" in
    daily)
      archive=public/snapshot/$(date +%Y/%m/%d)
      ;;
    release)
      archive=public/archive/${current_version}
      ;;
  esac
fi

# add files from source
config=src/$channel/conf/distributions
if [ ! -f "${config}" ]; then
  cp "${config}.orig" "${config}"
fi
rsync -avp src/ public/

# download seed if given; assumed to be a compressed archive (.tar or .zip),
# containing a directory called _build (may override artifacts from upstream
# jobs if used together with a trigger)
if [ -n "${LAVA_APT_SEED:-}" ]; then
  wget "${LAVA_APT_SEED}"
  seed=$(basename "${LAVA_APT_SEED}")
  case "$seed" in
    *.tar|*.tar.*)
      tar xafv $seed
      ;;
    *.zip|*)
      unzip -o $seed
      ;;
  esac
  rm -f $seed
fi

# inject incoming packages
if [ -d _build ]; then

  # archive older versions
  if [ -n "${archive}" ]; then
    mkdir -p "${archive}"
    cp -r "${basedir}"/dists "${basedir}"/pool "${archive}"/
  fi

  # include new packages
  for changes in _build/*.changes; do
    distro=$(grep-dctrl '' -n -s Distribution $changes)
    reprepro --basedir=$basedir include $distro $changes
  done

  # discard snapshot if no new version has been added
  new_version=$(reprepro -b ${basedir} ls lava | awk '{print($3); exit}' | cut -d + -f 1 || true)
  if [ "${new_version}" = "${current_version}" ]; then
    rm -rf "${archive}"
  fi
else
  # no packages to import; just re-export indices (to e.g. update repository
  # settings, or signing keys, remove obsolete packages etc)
  dists=$(reprepro -b "${basedir}" ls lava | awk '{print($5)}' || true)

  # remove obsolete packages
  for d in $dists; do
    reprepro --basedir=$basedir removesrcs $d libbpf bpfcc
  done

  # re-export all dists
  if [ -n "$dists" ]; then
    reprepro -b "${basedir}" export $dists
  fi
fi

discard_old() {
  set +x
  days="$1"
  dir="$2"
  fields="$3"
  repos=$(find "$dir" -name InRelease -mtime +${days} | cut -d / -f ${fields} | sort -u)
  for repo in $repos; do
    echo "I: deleting ${repo} (older than ${days} days)"
    rm -rf "${repo}"
    dir="$(dirname ${repo})"
    while [ "$(ls -1 "${dir}")" = "index.html" ]; do
      echo "I: deleting ${dir} (now empty)"
      rm -rf "${dir}"
      dir=$(dirname "${dir}")
    done
  done
  set -x
}

# discard old snapshots and archived releases
discard_old 7 public/snapshot/ 1-5
discard_old 365 public/archive/ 1-3


./fix-links.sh
./dedup.py

reprepro --basedir=$basedir check

cd public
find -mindepth 2 -name index.html -delete
tree -H . -I '*.js|*.css|*.html' -L 1 -T . > contents.html
find -mindepth 1 -type d -not '(' -name conf -or -name 'db' ')' -exec sh -c 'cd {} && tree -H . -I "conf|db|*.html" -L 1 -T "{}" > index.html' ';'
